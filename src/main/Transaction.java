package main;

import java.util.ArrayList;
import java.util.List;

public class Transaction{
	private String id;
	private List <String> listProduct;

	public String getId(){
		return id;
	}

	public void setId(String id){
		this.id = id;
	}

	public List <String> getListProduct(){
		return listProduct;
	}

	public void setListProduct(List <String> listProduct){
		this.listProduct = listProduct;
	}

	public Transaction(){
		listProduct = new ArrayList <>();
	}

	public Transaction(String id){
		this.id = id;
		listProduct = new ArrayList <>();
	}
}
