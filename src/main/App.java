package main;

import java.util.ArrayList;
import java.util.List;

public class App{
	/**
	 * Convert origin data to CSV(comma seperate value)
	 */
	public static void convertDataToCSVFormat(){
		List <String> productList = FileManager.readFile("products.txt");
		List <String> transactionList = FileManager.readFile("transactions.txt");
		List <String> productSymbol = FileManager.readFile("productSymbol.txt");
		List <String> output = new ArrayList <>();

		String t = "name";
		for(int i = 0; i < productSymbol.size(); i++)
			t += "," + productSymbol.get(i);
		output.add(t);

		for(int j = 0; j < transactionList.size(); j++){
			String temp = "T" + (j + 1);
			String[] split = transactionList.get(j).split(",");
			List <String> productInTransaction = new ArrayList <>();
			for(String s : split){
				productInTransaction.add(s.trim());
			}

			for(int i = 0; i < productList.size(); i++){
				if(productInTransaction.contains(productList.get(i)))
					temp += ",y";
				else temp += ",n";
			}
			output.add(temp);
		}

		for(String s :
				output){
			System.out.println(s);
		}

		FileManager.writeFile(output, "sales.csv");
	}

	public static void main(String[] args){
		convertDataToCSVFormat();
	}
}
