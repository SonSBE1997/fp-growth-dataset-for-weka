package main;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileManager{
	public static List <String> readFile(String fileName){
		List <String> list = new ArrayList <>();
		try{
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			String line;

			while((line = br.readLine()) != null){
				list.add(line);
			}

			br.close();
			fr.close();
		} catch(IOException e){
			System.out.println("error");
		}
		return list;
	}

	public static void writeFile(List <String> list, String fileName){
		try{
			FileWriter fw = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fw);
			for(String s : list){
				bw.write(s);
				bw.write("\r\n");
			}
			bw.close();
			fw.close();
		} catch(IOException e){
			System.out.println("error");
		}
	}
}
